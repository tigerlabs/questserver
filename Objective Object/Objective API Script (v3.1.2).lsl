// Max range 
key user;
integer i;
string msg;

//NC Variables
integer enforceRange;
string objectRole;
string configRole;
string name;
string message;
string objectNum;
string configNum;
integer serverChannel;
integer nextServerChannel;
string chainMode;
float groupRange;
integer groupMode;
string configurationNotecardName = "!API_CONFIG";
key notecardQueryId;
integer line;
integer configured = FALSE;
integer HUSH;
// Text Status
list cursor = ["◜ "," ◝"," ◞","◟ "];
string STATUS;
integer s = 0;
integer frame_max;
updateStatus(){
    if (s != frame_max){   
        llSetText(llList2String(cursor, s)+" "+STATUS, <1,1,1>,1);
        s++;
    } else {
        llSetText(llList2String(cursor, s)+" "+STATUS, <1,1,1>,1);
        s = 0;
    }
    
}

//Encryption 
string ProtocolSignature = "QSVR"; // your own signature
float ProtocolVersion = 0.3; // can range from 0.0 to 255.255
string Password; // change this to your own password
integer communicationsChannel = PUBLIC_CHANNEL;
string Header;
string strHex = "0123456789ABCDEF";

// Encrypt all communications
messageServer(integer channel, string message){
    llRegionSay(channel, encrypt(Password, message)); 
}

integer contains(string haystack, string needle) // http://wiki.secondlife.com/wiki/llSubStringIndex
{
    return ~llSubStringIndex(haystack, needle);
}

integer countSpaces(string input) {
    list explode = llParseString2List(input, [" "], ["."]); //Dump input in to a list
    integer length = llGetListLength(explode); //Count list length
    if (length > 0) {
        return length - 1;
    } else {
        return 0;
    }
} 
string hex(integer value)
{
    integer digit = value & 0xF;
    string text = llGetSubString(strHex, digit, digit);
    value = (value >> 4) & 0xfffFFFF;
    integer odd = TRUE;
    while(value)
    {
        digit = value & 0xF;
        text = llGetSubString(strHex, digit, digit) + text;
        odd = !odd;
        value = value >> 4;
    }
    if(odd)
        text = "0" + text;
    return text;
}
string encrypt(string password, string message)
{
    // get a random value
    integer nonce = (integer)llFrand(0x7FFFFFFF);
 
    // generate digest and prepend it to message
    message = llMD5String(message, nonce) + message;
 
    // generate one time pad
    string oneTimePad = llMD5String(password, nonce);
 
    // append pad until length matches or exceeds message
    integer count = (llStringLength(message) - 1) / 32;
    if(count)
        do
            oneTimePad += llMD5String(oneTimePad, nonce);
        while(--count);
 
    // return the header, nonce and encrypted message
    return Header + llGetSubString("00000000" + hex(nonce), -8, -1) + llXorBase64StringsCorrect(llStringToBase64(message), llStringToBase64(oneTimePad));
}

init() {

    frame_max = llGetListLength(cursor) - 1;
    STATUS = "Initialising...";
    updateStatus();
    //  reset configuration values to null
    objectNum = "";
    serverChannel = 0;
    groupRange = 0;
    groupMode = 0;
    objectRole = "";
    //  make sure the file exists and is a notecard
    if (llGetInventoryType(configurationNotecardName) != INVENTORY_NOTECARD) {
        //  notify owner of missing file
        llOwnerSay("Missing inventory notecard: " + configurationNotecardName);
        //  don't do anything else
        return;
    }

    //  initialize to start reading from first line (which is 0)
    line = 0;
    notecardQueryId = llGetNotecardLine(configurationNotecardName, line);
        // Cryptography 
    //build the header, it never changes.
    updateStatus();
    list versions = llParseString2List((string)ProtocolVersion, ["."], []);
    string minor = llList2String(versions, 1);
    integer p = 0;
    while(llGetSubString(minor, --p, p) == "0");
    Header = ProtocolSignature + hex(llList2Integer(versions, 0)) + hex((integer)llGetSubString(minor, 0xFF000000, p));            
    
    // End Cryptography
}

processConfiguration(string data) {
    //  if we are at the end of the file
    if (data == EOF) {
        //  notify the owner
        llOwnerSay("We are done reading the configuration");

        //  notify what was read
        llOwnerSay("========= CONFIG ==========");
        llOwnerSay("Objective Number: " + (string) objectNum);
        llOwnerSay("Server Channel: " + (string) serverChannel);
        llOwnerSay("Chain Mode: " + chainMode );
        llOwnerSay("Next Server Channel: " + (string) nextServerChannel);
        llOwnerSay("Object Role: " + (string) objectRole);
        llOwnerSay("(Group) Range: " + (string) llRound(groupRange));
        llOwnerSay("Group Mode: " + (string) groupMode);
        llOwnerSay("Enforce Range: "+ (string)enforceRange);
        llOwnerSay("Cipher: '"+Password+"'");
        if (Password == ""){
        llOwnerSay("NOTICE! - You have not set the Cipher in the config, this is needed to talk to the server. [Hint: It is the same cipher given to the Quest Server]");
        }
        //  do not do anything else
        llSetText("", < 1, 1, 1 > , 1);
        return;
    }

    //  if we are not working with a blank line
    if (data != "") {
        //  if the line does not begin with a comment
        if (llSubStringIndex(data, "#") != 0) {
            //  find first equal sign
            integer i = llSubStringIndex(data, "=");

            //  if line contains equal sign
            if (i != -1) {
                STATUS = "Loading...";
                updateStatus();
                //  get name of name/value pair
                string name = llGetSubString(data, 0, i - 1);

                //  get value of name/value pair
                string value = llGetSubString(data, i + 1, -1);

                //  trim name
                list temp = llParseString2List(name, [" "], []);
                name = llDumpList2String(temp, " ");

                //  make name lowercase (case insensitive)
                //name = llToLower(name);

                //  trim value
                temp = llParseString2List(value, [" "], []);
                value = llDumpList2String(temp, " ");

                // Objective Number
                if (name == "objectNum") {
                    objectNum = (string) value;
                    configNum = (string) value;
                } else
                   // Quest Server Channel
                if (name == "serverChannel") {
                   serverChannel = (integer) value;
                } else
                   // Group Mode
                if (name == "groupMode") {
                    groupMode = (integer) value;
                } else
                   // Scan range
                if (name == "groupRange") {
                    groupRange = (integer) value;
                } else
                if (name == "message") {
                    message = (string) value;
                } else
                if (name == "enforceRange") {
                    enforceRange = (integer) value;
                } else
                if (name == "cipher") {
                    Password = (string) value;
                } else
                if (name == "nextServerChannel") {
                    nextServerChannel = (integer) value;
                } else
                if (name == "chainMode") {
                    chainMode = value;
                } else
                if (name == "hush"){
                    if (value == "1"){
                        HUSH = TRUE;
                    } else {
                        HUSH = FALSE;
                    }
                } else
                if (name == "objectRole") {
                    if ((integer)value == 1){ 
                        objectRole = "Item,";
                        configRole = "Item,";
                    }else if ((integer)value == 2){
                        objectRole = "Initiator,";
                        configRole = "Initiator,";
                    }
                }
                //  unknown name
                else
                    llOwnerSay("Unknown configuration value: " + name + " on line " + (string) line);
            }
            //  line does not contain equal sign
            else {
                llOwnerSay("Configuration could not be read on line " + (string) line);
            }
        }
    }

    //  read the next line
    notecardQueryId = llGetNotecardLine(configurationNotecardName, ++line);
    configured = TRUE;
}

default {
    on_rez(integer start_param) {
        init();
        configured = FALSE;

    }
    changed(integer change) {
        configured = FALSE;
        if (change & (CHANGED_OWNER | CHANGED_INVENTORY)){
            init();
        }
    }

    state_entry() {
        if (!configured) {
            init();
        } else {
            llSetText("",<0,0,0>,0);

        }
        llSetTimerEvent(0);
    }
    dataserver(key request_id, string data) {
        if (request_id == notecardQueryId)
            processConfiguration(data);
    }
    link_message(integer Sender, integer OBJECT_NUMBER, string msg, key Key) {
        user = (key) msg;
    if(OBJECT_NUMBER >= -1 && OBJECT_NUMBER <= 6){
        if (OBJECT_NUMBER == -1 ) {
            //Set Text Function string,vector,float
            list p = llCSV2List(msg);
            llSetText(llBase64ToString(llList2String(p,0)),(vector)llList2String(p,1),(float)llList2String(p,2)); 
               
        } else {
        if (OBJECT_NUMBER >= 1 && OBJECT_NUMBER <= 5 ) {
            objectRole = "Item,";
            objectNum = (string)OBJECT_NUMBER;
        } 
        if (OBJECT_NUMBER == 6){
            objectRole = "Initiator,";    
        }  
        if (OBJECT_NUMBER < -1 | OBJECT_NUMBER > 6 ) {
         //   llOwnerSay("Illegal Number parsed to API '"+(string)OBJECT_NUMBER+"' - OBJECT_NUMBER must be between 0 and 6, using config setting instead.");
            objectRole = configRole;
            objectNum = configNum;
        }
     // get the position of the avatar touching this prim
        vector pos = llList2Vector(llGetObjectDetails(user, ([OBJECT_POS])),0);
     
        // compute how far away they are
        float dist = llVecDist(pos, llGetPos() );
        
        //Make sure user is within range first
        if (dist > groupRange && enforceRange) {
                if (!HUSH){llInstantMessage(user, "You are too far away");}
            }else {       
                if (!HUSH){llInstantMessage(user, message);}
            if (groupMode == 1){
                state process;
            }else{
                if (chainMode == "1") { messageServer(nextServerChannel, "Initiator," + (string) user + "," + objectNum); }
                messageServer(serverChannel, objectRole + (string) user + "," + objectNum);
            }
        }
        }
     }   
    }
}
state process {
    //Process Group mode
    state_entry() {
        i = 15;
        llSetTimerEvent(1);
    }
    timer() {
        if (i > 0){
        STATUS = "\nGroup Credit Mode! \n Please stand within "+(string)llRound(groupRange)+"M of objective. \n Scanning in "+(string)i+"s";
        updateStatus();
        i--;
        } else {
        llSetTimerEvent(0);
        list keys = llGetAgentList(AGENT_LIST_REGION, []);
        integer numberOfKeys = llGetListLength(keys);
 
        vector currentPos = llGetPos();
        list newkeys;
        key thisAvKey;
 
        integer i;
        for (i = 0; i < numberOfKeys; ++i) {
            thisAvKey = llList2Key(keys,i);
            newkeys += [llRound(llVecDist(currentPos,
                            llList2Vector(llGetObjectDetails(thisAvKey, [OBJECT_POS]), 0))),
                        thisAvKey];
        }
 
        newkeys = llListSort(newkeys, 2, FALSE);     //  sort strided list by descending distance
 
        for (i = 0; i < (numberOfKeys * 2); i += 2) {
            if ((integer)llList2Integer(newkeys, i) <= llRound(groupRange)){
                llSay(0,"Credited "+llGetDisplayName(llList2Key(newkeys, i+1))+" ["+ (string) llList2Integer(newkeys, i) + "m]"); 
                if (chainMode == "1") { messageServer(nextServerChannel, "Initiator," + (string) llList2Key(newkeys, i+1) + "," + objectNum); }
                STATUS = "Sending List...["+(string)i+"]";
                updateStatus();
                messageServer(serverChannel, objectRole + (string) llList2Key(newkeys, i+1) + "," + objectNum);                
            }
        }
        state default;
        } 

    }
}
integer LINK_DISPENSE = 4;
integer LINK_BUFFER = 3;
integer LINK_SVR = 5;
integer LINK_DB = 6;
integer LINK_XP = 8;
string url;
string apiSvr = "";
integer DEBUG;
integer REZ;
key urlRequestId;
integer gListener; 
string configurationNotecardName = "CONFIG";
key notecardQueryId;
key url_request;
string api = apiSvr+"/?token=";
integer line;
integer listenChannel;
integer webAdmin = FALSE;
string HUSH;
string serverPort;
string databaseName;
string dbsecret;
string cipher;
string rewardMode;
string reward;
string cap;
string hudpassword;
string nexusAPI;
string start_item;
string end_item;
string strictOrder;
string reportProgress;
string instance;
string xpType;
key ownerUUID;
key serverUUID; 
string label; 
string chatName;
integer READING;
integer AWS;
//slave mode
string slaveMode;
string masterServerChannel;
string slaveEnroll;
string masterCipher;
string slaveObjectiveNumber;
integer errors = FALSE;
list cursor = [
            "⢀⠀",
            "⡀⠀",
            "⠄⠀",
            "⢂⠀",
            "⡂⠀",
            "⠅⠀",
            "⢃⠀",
            "⡃⠀",
            "⠍⠀",
            "⢋⠀",
            "⡋⠀",
            "⠍⠁",
            "⢋⠁",
            "⡋⠁",
            "⠍⠉",
            "⠋⠉",
            "⠋⠉",
            "⠉⠙",
            "⠉⠙",
            "⠉⠩",
            "⠈⢙",
            "⠈⡙",
            "⢈⠩",
            "⡀⢙",
            "⠄⡙",
            "⢂⠩",
            "⡂⢘",
            "⠅⡘",
            "⢃⠨",
            "⡃⢐",
            "⠍⡐",
            "⢋⠠",
            "⡋⢀",
            "⠍⡁",
            "⢋⠁",
            "⡋⠁",
            "⠍⠉",
            "⠋⠉",
            "⠋⠉",
            "⠉⠙",
            "⠉⠙",
            "⠉⠩",
            "⠈⢙",
            "⠈⡙",
            "⠈⠩",
            "⠀⢙",
            "⠀⡙",
            "⠀⠩",
            "⠀⢘",
            "⠀⡘",
            "⠀⠨",
            "⠀⢐",
            "⠀⡐",
            "⠀⠠",
            "⠀⢀",
            "⠀⡀"
            ];

string STATUS;
integer i = 0;
integer frame_max;
integer HTTP_TIMEOUT = -1; //ms - 5 minutes
integer LISTEN_TIMEOUT = -1; //-1 for disabled. 
integer listenTimer = FALSE;
updateStatus(){
    if (i != frame_max){   
        llSetText(llList2String(cursor, i)+" "+STATUS, <1,1,1>,1);
        i++;
    } else {
        llSetText(llList2String(cursor, i)+" "+STATUS, <1,1,1>,1);
        i = 0;
    }
    
}
request_url()
{
    llReleaseURL(url);
    url = "";
    urlRequestId = llRequestURL();
}
updateModules(integer channel){
    if(!READING){
        STATUS = "Reconfiguring...";
        if (channel == 0) {
            channel = LINK_SET;
        }
        STATUS = "Updating Modules";
        llPlaySound("25119931-a93c-3dbf-8928-2e00ff2caaba",0.5);
        llOwnerSay("Updating Modules...");
        llMessageLinked(channel, 0, "CONFIG", "");
        llSleep(2); //sleep and allow scripts to enter config mode
        llMessageLinked(channel, 0, "hush," + HUSH, NULL_KEY);                                  llSleep(.25);
        llMessageLinked(channel, 0, "channel," + serverPort, NULL_KEY);                         llSleep(.25);
        llMessageLinked(channel, 0, "apiSvr," + apiSvr, NULL_KEY);                              llSleep(.25); // must be sent before db
        llMessageLinked(channel, 0, "db," + databaseName, NULL_KEY);                            llSleep(.25); // must be sent after apiSvr
        llMessageLinked(channel, 0, "strict_order," + strictOrder, NULL_KEY);                   llSleep(.25);
        llMessageLinked(channel, 0, "report_progress," + reportProgress, NULL_KEY);             llSleep(.25);
        llMessageLinked(channel, 0, "key," + dbsecret, NULL_KEY);                               llSleep(.25);
        llMessageLinked(channel, 0, "cipher," + cipher, NULL_KEY);                              llSleep(.25);
        llMessageLinked(channel, 0, "reward,"+ reward, NULL_KEY);                               llSleep(.25);
        llMessageLinked(channel, 0, "hudpassword,"+ hudpassword, NULL_KEY);                     llSleep(.25);
        llMessageLinked(channel, 0, "instance,"+ instance, NULL_KEY);                           llSleep(.25);
        llMessageLinked(channel, 0, "xptype,"+ xpType, NULL_KEY);                               llSleep(.25);
        llMessageLinked(channel, 0, "nexus_api,"+ nexusAPI, NULL_KEY);                          llSleep(.25);
        llMessageLinked(channel, 0, "start_item,"+ start_item, NULL_KEY);                       llSleep(.25);
        llMessageLinked(channel, 0, "end_item,"+ end_item, NULL_KEY);                           llSleep(.25);
        llMessageLinked(channel, 0, "slaveMode,"+slaveMode, NULL_KEY);                          llSleep(.25);
        llMessageLinked(channel, 0, "masterServerChannel,"+masterServerChannel, NULL_KEY);      llSleep(.25);
        llMessageLinked(channel, 0, "slaveEnroll,"+slaveEnroll, NULL_KEY);                      llSleep(.25);
        llMessageLinked(channel, 0, "masterCipher,"+masterCipher, NULL_KEY);                    llSleep(.25);
        llMessageLinked(channel, 0, "slaveObjectiveNumber,"+slaveObjectiveNumber, NULL_KEY);    llSleep(.25);
        llMessageLinked(channel, 0, "rewardMode,"+rewardMode, NULL_KEY);                        llSleep(.25);
        llMessageLinked(channel, 0, "chatName,"+chatName, NULL_KEY);                            llSleep(.25);
        llMessageLinked(channel, 0, "cap,"+cap, NULL_KEY);                                      llSleep(.25);
        llOwnerSay("Initialising...");
        STATUS = "Initialising...";
        llMessageLinked(channel, 0, "INIT", "");
        llPlaySound("905bfb1e-fb77-0382-5f94-44fc85d6e48e",0.5);
        llOwnerSay("OK.");
        STATUS = "";
    } else {
        llOwnerSay("Please wait for the config to be loaded before attempting to apply it.");
    }
            
}

setDebugLevel(integer level){
        DEBUG = level;
        integer channel = LINK_SET;
        llPlaySound("25119931-a93c-3dbf-8928-2e00ff2caaba",0.5);
        llOwnerSay("Configuring Modules...");
        llMessageLinked(channel, 0, "CONFIG", "");
        llSleep(1); //sleep and allow scripts to enter config mode
        llMessageLinked(channel, 0, "debug," + (string)level, NULL_KEY);
        llOwnerSay("Initialising...");
        llMessageLinked(channel, 0, "INIT", "");
        llPlaySound("905bfb1e-fb77-0382-5f94-44fc85d6e48e",0.5);
        llOwnerSay("OK.");   
}

init()
{
    errors = FALSE;
    llOwnerSay("Loading Notecard...");
    READING = TRUE;
    llPlaySound("537c26e2-4515-895a-7d54-720b84612150", 0.5);
    serverUUID = llGetKey();
    ownerUUID = llGetOwner();
    databaseName = (string)serverUUID;

 
//  make sure the file exists and is a notecard
    if(llGetInventoryType(configurationNotecardName) != INVENTORY_NOTECARD)
    {
    //  notify owner of missing file
        llOwnerSay("Missing inventory notecard: " + configurationNotecardName);
    //  don't do anything else
        return;
    }
 
//  initialize to start reading from first line (which is 0)
    line = 0;
    notecardQueryId = llGetNotecardLine(configurationNotecardName, line);
}
 
processConfiguration(string data)
{
    STATUS = "Reading Notecard...";
//  if we are at the end of the file
    if(data == EOF)
    {
        if (!errors){
        READING = FALSE;
        //  notify what was read
        llOwnerSay("========CONFIG========");
        llOwnerSay("Database APIK: " + dbsecret);
        llOwnerSay("Comms Cipher: '" + cipher+"'");
        llOwnerSay("Start Item: " + start_item);
        llOwnerSay("End Item: "+end_item);
        llOwnerSay("Server Channel: " + serverPort);
        llOwnerSay("Chat Name: ["+chatName+"]");
        llOwnerSay("Strict Order: "+strictOrder);
        llOwnerSay("Report Progress: "+reportProgress);
        llOwnerSay("Stealth Mode (Hush): "+HUSH); 
        llOwnerSay("======REWARD MODE=====");
        if((integer)rewardMode > 0){
        llOwnerSay("Reward mode: "+rewardMode);
        llOwnerSay("Reward amount: " + reward);
        if((integer)rewardMode == 1){
        llOwnerSay("Hud Pw: " + hudpassword);
        }
        if((integer)rewardMode == 2){
        llOwnerSay("Reward cap: " + cap);
        }
        if((integer)rewardMode == 3){
        llOwnerSay("Nexus API Key: " + nexusAPI);
        }
        if((integer)rewardMode == 4){
        llOwnerSay("GPHud Instance: " + instance);
        llOwnerSay("GPHud XP Type: "+xpType);
        }
        } else {
            llOwnerSay("....................[DISABLED]");
        }
        llOwnerSay("======SLAVE MODE======");
        if ((integer)slaveMode > 0){
        llOwnerSay("Slave Mode: "+slaveMode);
        llOwnerSay("Master Svr Ch: "+masterServerChannel);
        llOwnerSay("Slave Enroll: "+slaveEnroll);
        llOwnerSay("Master Cipher: "+masterCipher);
        llOwnerSay("slave Objective Number: "+slaveObjectiveNumber);
        } else {
        llOwnerSay("....................[DISABLED]");
        }
        llOwnerSay("========CONFIG========\n");
        }
    
    //  do not do anything else
        if (dbsecret == "" | cipher == ""){
         llOwnerSay("This server has not been set up! please click on the main cabinet and select 'Admin' from the dialogue to configure this server.");  
        STATUS = "Setup Required!"; 
        } else 
        if (errors){
            llOwnerSay("There were errors in your config, please read above and try again");
            llPlaySound("3b54b31b-9e16-0b9c-0cf9-53ef35b33903",1.0);
            STATUS = "ERROR!"; 
        } else {
            llOwnerSay("Notecard config read, please click on the server and select [Reconfigure] to apply the changes.");  
            REZ = FALSE;
            llPlaySound("9b538310-9cf6-693c-79ff-b7dfaf06ddb7",0.5);
            STATUS = ""; 
        }
    
        return;
    }
    
 
//  if we are not working with a blank line
    if(data != "")
    {
    //  if the line does not begin with a comment
        if(llSubStringIndex(data, "#") != 0)
        {
        //  find first equal sign
            integer i = llSubStringIndex(data, "=");
 
        //  if line contains equal sign
            if(i != -1)
            {
            //  get name of name/value pair
                string name = llGetSubString(data, 0, i - 1);
 
            //  get value of name/value pair
                string value = llGetSubString(data, i + 1, -1);
 
            //  trim name
                list temp = llParseString2List(name, [" "], []);
                name = llDumpList2String(temp, " ");
 
            //  make name lowercase (case insensitive)
                name = llToLower(name);
 
            //  trim value
                temp = llParseString2List(value, [" "], []);
                value = llDumpList2String(temp, " ");
                if(name == "hush"){
                    HUSH = value;
                    jump read;
            //  channel
                }else if(name == "channel"){
                    serverPort = value;
                    jump read;  
            //  force progression
                }else if(name == "strictorder"){
                    strictOrder = value;
                    jump read; 
            //  report Progress
                }else if(name == "reportprogress"){
                    reportProgress = value;
                    jump read; 
                }else if(name == "label"){
                    label = value;
                    jump read;                     
            //  dbKey
                }else if(name == "apikey"){
                    dbsecret = value;
                    jump read; 
            //  chatName
                }else if (name =="apisvr"){
                    apiSvr = value;
                    api = apiSvr+"/?token="; //ensure url is complete
                    jump read;
                }else if(name == "chatname"){
                    chatName = value;
                    jump read; 
            //  cipher
                }else if(name == "cipher"){
                    cipher = value;
                    jump read; 
             // Reward mode
                }else if(name == "rewardmode"){
                    rewardMode = value;
                    jump read; 
             // Reward
                }else if(name == "reward"){
                    reward = value;
                    jump read; 
             // Reward cap
                }else if(name == "cap"){
                    cap = value;
                    jump read; 
             //  HUD Password
                }else if(name == "hudpassword"){
                    hudpassword = value;
                    jump read; 
             //  GPHud Instance Name
                }else if(name == "instance"){
                    instance = value;
                    jump read; 
             //  GPHud XP Type
                }else if(name == "xptype"){
                    xpType = value;
                    jump read; 
            //  Nexus API Key
                }else if(name == "nexusapi"){
                    nexusAPI = value;
                    jump read; 
             //  start item
                }else if(name == "start_item"){
                    start_item = value;
                    jump read; 
             //  end item
                }else if(name == "end_item"){
                    end_item = value;
                    jump read; 
            //  slave mode vars
                }else if (name == "slavemode"){
                    slaveMode = value;
                    jump read; 
                }else if (name == "masterserverchannel"){
                    masterServerChannel = value;
                    jump read; 
                }else if (name == "slaveenroll"){
                    slaveEnroll = value;
                    jump read; 
                }else if (name == "mastercipher"){
                    masterCipher = value;
                    jump read; 
                }else if (name == "slaveobjectivenumber"){
                    slaveObjectiveNumber = value;
                    jump read; 
            //  unknown name
                }else {
                    llOwnerSay("Unknown configuration value: " + name + " on line " + (string)line);
                        errors = TRUE;
                    }
            }
        //  line does not contain equal sign
            else
            {
                llOwnerSay("Configuration could not be read (missing = between name and value?) on line " + (string)line);
                errors = TRUE;
            }
        }
    }
    @read; //really only did this in the vain hope we could at least compensate for the lack of case statements in lsl.. though it only nets .1s on a healthy sim.
//  read the next line
    notecardQueryId = llGetNotecardLine(configurationNotecardName, ++line);
}
 
default
{
    on_rez(integer start_param)
    {
        REZ = TRUE;
        init();
    }
 
    changed(integer change)
    {
        if (change & CHANGED_OWNER){
            llMessageLinked(0, 0, "CONFIG", "");
            llResetScript();

        }
        if (change & CHANGED_INVENTORY){
            llListenRemove(gListener);
            listenChannel = llFloor(llFrand(-9999)) + 9999;
            key user = llGetOwner();
            gListener = llListen(listenChannel, "", user, "");
            llDialog(user, "\nAn Inventory change was detected, would you like to reload the config? \n\n (Ignore if you did not change the config)", ["Reload", "Ignore"] , listenChannel);
        }
        
    }
 
    state_entry()
    {
        frame_max = llGetListLength(cursor) - 1;
        llSetTimerEvent(0.1);
        llPreloadSound("3b54b31b-9e16-0b9c-0cf9-53ef35b33903");
        llPreloadSound("25119931-a93c-3dbf-8928-2e00ff2caaba");
        llPreloadSound("537c26e2-4515-895a-7d54-720b84612150");
        llPreloadSound("905bfb1e-fb77-0382-5f94-44fc85d6e48e");
        llPreloadSound("9b538310-9cf6-693c-79ff-b7dfaf06ddb7");
        init();
    }
    touch_start(integer total_number)
    {
     if (llDetectedKey(0) == llGetOwner()){
        // Kill off any outstanding listener, to avoid any chance of multiple listeners being active
        llListenRemove(gListener);
        listenChannel = llFloor(llFrand(-9999)) + 9999;
        // get the UUID of the person touching this prim
        key user = llDetectedKey(0);
        // Listen to any reply from that user only, and only on the same channel to be used by llDialog
        // It's best to set up the listener before issuing the dialog
        gListener = llListen(listenChannel, "", user, "");
        // Send a dialog to that person. We'll use a fixed negative channel number for simplicity
        llDialog(user, "\nSelect an option", ["Reconfigure", "Reload", "Admin", "Help Token", "Debug"] , listenChannel);
        // Start a one-minute timer, after which we will stop listening for responses
        LISTEN_TIMEOUT = 3000;
        listenTimer = TRUE;
     }
    }
    link_message(integer sender_num, integer num, string msg, key id) {
     if (!REZ){
        if (msg == "CONFIG_GET"){
          updateModules(sender_num);
        }
     }
    }
    listen(integer chan, string name, key id, string msg)
    {
        if (msg == "Reconfigure"){
            STATUS = "Updating Modules...";
            updateModules(0);
        } else if (msg == "Reload"){
            llMessageLinked(0, 0, "CONFIG", "");
            init();
        } else if (msg == "Admin"){
            llReleaseURL(url);
            request_url(); 
            HTTP_TIMEOUT = 3000;           
            webAdmin = TRUE;
        } else if (msg == "Help Token"){
            llOwnerSay("Plase provide the following text in your support request: "+llStringToBase64((string)llGetOwner()+" | "+(string)llGetKey()));
        } else if (msg == "Debug"){
            // Debug Menu
            llDialog(id, "\nSet Debug Level \n None - All off, no hover text \n Minimal - Hover text only \n Full - Spam me Senpai \n\n                             !!!WARNING!!!  \n Changing this while the server is processing a queue can cause scripts to freeze.", ["None", "Minimal", "Full"] , listenChannel);
        } else if (msg == "None"){
            setDebugLevel(0);
            llInstantMessage(id, "All Debugging Disabled");
        } else if (msg == "Minimal"){
            setDebugLevel(1);
            llInstantMessage(id, "Debigging set to Hover text only");
        } else if (msg == "Full"){
            setDebugLevel(2);
            llInstantMessage(id, "All debugging at max verbosity, Spam me senpai!");
        }else{
            //llSetTimerEvent(0.1);
            LISTEN_TIMEOUT=10;
            listenTimer = TRUE;
            
        }
    }
    timer()
    {
        // Timer will fire in .1 second ticks for display
        // 1 rt second is 10 ticks. 
        if (webAdmin){
            
            if (HTTP_TIMEOUT > 0){
                HTTP_TIMEOUT--;
                STATUS = "Web Admin Active! \n Token Expires in: "+(string)(HTTP_TIMEOUT / 10)+"s. \n"+label;
            } else if (HTTP_TIMEOUT == 0){
                STATUS = "";
                llReleaseURL(url);
                HTTP_TIMEOUT= -1;
                webAdmin = FALSE;
            } else {
                // Do nothing
            }
        } 
        // Stop listening. It's wise to do this to reduce lag
        if (listenTimer){
            if (LISTEN_TIMEOUT > 0){
                LISTEN_TIMEOUT--;
            } else if (LISTEN_TIMEOUT == 0){
                llListenRemove(gListener);
                LISTEN_TIMEOUT= -1;
                listenTimer = FALSE;
            } else {
                //Do nothing
            }
        } 
        if (STATUS != "") { updateStatus(); } else { llSetText(label, <1,1,1>,1.0); }
        
    }
 
    dataserver(key request_id, string data)
    {
        if(request_id == notecardQueryId)
            processConfiguration(data);
    }
    http_request(key id, string method, string body)
    {
        key owner = llGetOwner();
        vector ownerSize = llGetAgentSize(owner);
 
        if (urlRequestId == id)
        { 
            if (method == URL_REQUEST_GRANTED)
            {  
               // generate our token injected api url
               list parts = llParseString2List((string)body,["/", "."],[""]);
               // Our url ends up as a list in this order
               // <http:><sim10371><agni><lindenlab><com:12046><cap><027d169b-5680-1202-5b55-7389aaa8eaad>
               //   0        1        2      3         4         5               6 
               // we want values 1 and 6
               //Assemble token as a CSV string, we will regenerate the full url in php
               string token = llList2String(parts,1)+","+llList2String(parts,6);
               if (llList2String(parts,4) == "io:12046"){AWS = TRUE; llOwnerSay("AWS Region Detected");} //AWS Region Compatability fix
               //Now encode it with base64
               if (AWS){ token += ",aws"; }
               token = (string)llStringToBase64(token);
               
                llOwnerSay("URL: " + api + token);
                url = body;
 
              //if owner in sim
               if (ownerSize){//  != ZERO_VECTOR
                    llLoadURL(owner, "[QuestServer] Database Admin", api + token);
                }
            }
 
            else if (method == URL_REQUEST_DENIED)
                llOwnerSay("Something went wrong, no url:\n" + body);
        }
 
        else
        {
            //llOwnerSay("request body:\n" + body);
            //llSetTimerEvent(300);
            HTTP_TIMEOUT = 3000; //reset the http counter.
            //  if owner in sim
            if (ownerSize)//  != ZERO_VECTOR
            {
                llSetContentType(id, CONTENT_TYPE_TEXT);
                string response = (string)llStringToBase64(llKey2Name(llGetOwner())+","+(string)llGetOwner()+","+(string)llGetKey());
                llHTTPResponse(id, 200, response);
            }
            else
            {
                llSetContentType(id, CONTENT_TYPE_TEXT);
                llHTTPResponse(id, 200, "OK");
            }
        }
    }
}

integer listenChannel;

integer configured = FALSE;
integer err = FALSE;
integer LINK_DISPENSE = 4;
integer LINK_BUFFER = 3;
integer LINK_SVR = 5;
integer LINK_DB = 6;
integer LINK_XP = 8;
list queue = [];
string chat;
integer queueWait = FALSE;
string serviceState;
integer debug;
//Cryptography
string ProtocolSignature = "QSVR"; // your own signature
float ProtocolVersion = 0.3; // can range from 0.0 to 255.255
string Password;
integer communicationsChannel = PUBLIC_CHANNEL;
integer Debug = TRUE; // Set this to false for production
integer listener;
init()
{
    if(listener != 0)
    {
        llListenRemove(listener);
        listener = 0;
    }
    listener = llListen(communicationsChannel, "", NULL_KEY, "");
}
string error(string message)
{
    if(Debug) llSay(DEBUG_CHANNEL, message);
    err = TRUE;
    return "";
}
string decrypt(string password, string message)
{
    integer signatureLength = llStringLength(ProtocolSignature);
    integer headerLength = signatureLength + 12; // version = 4, nonce = 8
 
    // verify length of encrypted message
    if(llStringLength(message) < signatureLength + 44) // digest = 32 (base64 = 44) + at least one character
        return error("Invalid Message Length, this could be something other than the API using this channel.");
 
    // look for protocol signature in message header
    if(llSubStringIndex(message, ProtocolSignature) != 0)
        return error("Unknown protocol.");
 
    // Parse version information from header
    integer index = signatureLength; // determine where to start parsing
    string major = "0x" + llGetSubString(message, index, ++index);
    string minor = "0x" + llGetSubString(message, ++index, ++index);
    float version = (float)((string)((integer)major) + "." + (string)((integer)minor));
 
    // verify version is supported
    if(version != ProtocolVersion)
        return error("Unknown version.");
 
    // parse nonce from header
    integer nonce = (integer)("0x" + llGetSubString(message, ++index, index + 7));
 
    // remove header from message
    message = llGetSubString(message, headerLength, -1);
 
    // create one time pad from password and nonce
    string oneTimePad = llMD5String(password, nonce);
    // append pad until length matches or exceeds message
    while(llStringLength(oneTimePad) < (llStringLength(message) / 2 * 3))
        oneTimePad += llMD5String(oneTimePad, nonce);
 
    // decrypt message
    oneTimePad = llStringToBase64(oneTimePad);
    message = llXorBase64StringsCorrect(message, oneTimePad);
 
    // decode message
    message = llBase64ToString(message);
 
    // get digest
    string digest = llGetSubString(message, 0, 31);
 
    // remove digest from message
    message = llGetSubString(message, 32, -1);
 
    // verify digest is valid
    if(llMD5String(message, nonce) != digest)
        return error("Message digest was not valid. Check that your config is correct");
 
    // return decrypted message
    return message;
}

default
{
    state_entry()
    {
        if (!configured){
            llMessageLinked(LINK_ROOT, 0, "CONFIG_GET", "");
            state config;
        } else {
            llListen(listenChannel,"","","");
            llSetTimerEvent(1);
        }
    }
    listen(integer channel, string name, key id, string msg)
    {
        msg = decrypt(Password,msg);
        if (!err){ 
            string data = msg+","+(string)id;
            queue += data; //msg; 
            if (debug == 2) llSay(DEBUG_CHANNEL,"Got valid data on channel "+(string) channel+", Adding to queue.");
        } else {
            msg = "";
            llSetText("DROPPED!",<1,0,0>,1);
            if (debug == 2) llSay(DEBUG_CHANNEL,"ERROR: Message was invalid and not submitted to queue, Is the ciper correct?");
            err = FALSE;
        }
    }
    
    link_message(integer sender_num, integer num, string msg, key id) {
        if (msg == "CONFIG"){ state config; }
        if (msg == "READY"){
            queueWait = FALSE;
            if (debug == 2) llSay(DEBUG_CHANNEL,"Got Ready signal from Logic Unit, Sending next in queue if not empty.");
        }
    }
    
    timer()
    {
        //Display some info like uptime, queue len etc.
        if (debug >= 1){
        llSetText("Buffer Running \n"
                  +"\nQueue: "+(string)llGetListLength(queue)
                  +"\nStatus: "+serviceState
                  + "\nFree: "+(string)llGetFreeMemory()+"K", 
                  <1,1,1>, 
                  1.0
                  );
        } else {
            if (llGetListLength(queue) > 0){
                llSetText("Queue: "+(string)llGetListLength(queue), <.5,.5,.5>,1);
            } else {
                llSetText("", <0,0,0>,0);
            }
        }
        // Process queue
        if (llGetListLength(queue) > 0 && !queueWait){
            serviceState = "PROCESSING";
            //fetch first item in list
            string item = llList2String(queue,0);
            queueWait = TRUE; //halt further processes until server is ready
           llMessageLinked(LINK_SVR, 0, llList2String(queue, 0), NULL_KEY);
            queue = llDeleteSubList(queue,0 ,0); //remove position 0
        } else if (llGetListLength(queue) > 0 && queueWait ){
            serviceState = "WAITING";
        } else {
            serviceState = "IDLE";
        }
    }
}

state config {
    state_entry(){
        llSetText("!CONFIG", <1,0,0>,1.0);   
    }
    link_message(integer sender_num, integer num, string msg, key id) {
        list data = llCSV2List(msg);
        string var = llList2String(data, 0);
        string val = llList2String(data, 1);
        
        if (var == "channel"){
            listenChannel = (integer)val;
        }
        if (var == "cipher"){
            Password = val;
        }
        if (var == "debug"){
            debug = (integer)val;    
        }
        if (var == "INIT"){
            configured = TRUE;
            state default;
        }
        
    }
}
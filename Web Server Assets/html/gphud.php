<?php
//GPHud Dev Key
$key = '######################';

//Header data
$headerList = [];
foreach ($_SERVER as $name => $value) {
	if (preg_match('/^HTTP_/',$name)) {
		// convert HTTP_HEADER_NAME to Header-Name
		$name = strtr(substr($name,5),'_',' ');
		$name = ucwords(strtolower($name));
		$name = strtr($name,' ','-');
		// add to list
		$headerList[$name] = $value;
	}
}
$HEADER_OWNER 	= $headerList['X-Secondlife-Owner-Key'];
// get vars from XP module. 
$OWNERSHA1      = $_GET['owner'];
$SLNAME         = $_GET['slname'];
$REWARD         = $_GET['reward'];
$INSTANCE       = $_GET['instance'];
$XPTYPE         = $_GET['xptype'];
//API Key / secret
//InstanceUUID and API Key
$TABLE          = $_GET['table'];
$AUTH_SECRET    = $_GET['secret'];

$OWNER = base64_decode($OWNERSHA1);

if ($OWNER == sha1($HEADER_OWNER)){
	//require("./api/".$TABLE."/vars.php");
	(@include_once ("./api/".$TABLE."/vars.php")) or die ('
	{
		"terminate": "BAD INSTANCE",
		"responsetype": "TerminateResponse"
	}    
	'); // no table, no page.

	// we aquire $secret from above. just double check this matches up too.
	if ($AUTH_SECRET == $secret){

		// begin transaction

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://sl.coagulate.net/GPHUD/external");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$vars = [
		'developerkey' => $key,
		'runasinstancename' => $INSTANCE,
		'applicationname' => 'QuestServer',
		'command' => 'experience.award', 
		'target' => ">".$SLNAME, 
		'runascharactername' => $SLNAME, 
		'type' => $XPTYPE,
		'ammount' => $REWARD,
		'reason' => 'Completing A Quest', 
		'checkavatarpermission' => $HEADER_OWNER
		];

		curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($vars));  //Post Fields
		$server_output = curl_exec ($ch);
		curl_close ($ch);
		print  $server_output ;


	} else {
		die ('
		{
			"terminate": "BAD API KEY",
			"responsetype": "TerminateResponse"
		}    
		');
	}

} else {
	die ('
    {
        "terminate": "OWNER",
        "responsetype": "TerminateResponse"
    }    
    ');
}

?>

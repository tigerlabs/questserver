// Confirm delete?
$('.delete').click(function(e){
    if(confirm('WARNING! This operation cannot be undone, Are you sure?')){
        // The user pressed OK
        // Do nothing, the link will continue to be opened normally
    } else {
        // The user pressed Cancel, so prevent the link from opening
        e.preventDefault();
    }
});
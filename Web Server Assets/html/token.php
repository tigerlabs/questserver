<?php 

// Example url from script http://sim10371.agni.lindenlab.com:12046/cap/1806e883-5ec8-0287-8737-1eb567df05e8
//                 we need       [        ]                             [                                   ]

//Decode our token
$token = $_GET['token'];
$string = base64_decode($token);
//Assemble a query
list($aws,$second,$first) = explode(',',strrev($string),3);
$first = strrev($first);
$second = strrev($second);
$aws = strrev($aws);
//AWS Url totally borked us, if it's an AWS region the token now has 3 values not 2, however if it's an older server on a legacy region still.. (else)
if ($aws == "aws"){
    $url = "http://".$first.".agni.secondlife.io:12046/cap/".$second;
} else {
    // Re-do the decode.. Backwards compatability fix and workaround for old regions and servers
    list($second,$first) = explode(',',strrev($string),2);
    $first = strrev($first);
    $second = strrev($second);
    $url = "http://".$first.".agni.lindenlab.com:12046/cap/".$second;
}

//make query to Quest Server
$lslResponse = file_get_contents($url);
list($third, $second,$first) = explode(',',strrev(base64_decode($lslResponse)),3);
//Define our values
$slName = strrev($first);
$ownerKey = strrev($second);
$serverKey = strrev($third);

//Token handshake complete. 

//Check token validity, if our values are null, something is wrong, 
if (!$slName && !$ownerKey && !$serverKey){
  //  die("TOKEN INVALID OR EXPIRED!!!");
  $message = "Link Invalid or Expired";
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>[QuestServer] Database Managment</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Dark.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
    <div class="login-dark">
        <form method="post">
            <h2 class="sr-only">Token Invalid</h2>
            <div class="illustration"><i class="icon ion-ios-locked-outline"></i></div>
            <div class="form-group">
                <h4 class="text-center"><?php echo($message);?></h4>
            </div>
            
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>
<?php
die();


}
// var_dump($slName);
// var_dump($ownerKey);
// var_dump($serverKey);
// echo ("Token OK");


?>
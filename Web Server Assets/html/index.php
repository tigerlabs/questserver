<?php 
function RandomString()
{
$permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

return substr(str_shuffle($permitted_chars), 0, 16);;
}

include("token.php");

$dbhost = '127.0.0.1';
$dbuser = 'sldb';
$dbpass = '##############';
$dbname = 'sldb';

$con = mysqli_connect($dbhost, $dbuser, $dbpass) or die ('ERROR: CANNOT CONNECT TO DATABASE.');
mysqli_select_db($con, $dbname) or die('ERROR: CANNOT SELECT DATABASE.');


$query = "select 1 from `$serverKey` LIMIT 1";
$val = mysqli_query($GLOBALS['con'], $query);
if($val !== FALSE)
{
 $message = "Welcome Back";
} else {

// new Database
$newServerTable = "
CREATE TABLE `".$serverKey."` (
`id` int(11) NOT NULL,
`uuid` varchar(36) DEFAULT NULL,
`field` varchar(100) DEFAULT NULL,
`value` varchar(1024) DEFAULT NULL,
`timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
";

$houseKeeping1 = "
ALTER TABLE `".$serverKey."`
ADD PRIMARY KEY (`id`);
";
$houseKeeping2 = "
ALTER TABLE `".$serverKey."`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";

$val = mysqli_query($GLOBALS['con'], $newServerTable);
$val = mysqli_query($GLOBALS['con'], $houseKeeping1);
$val = mysqli_query($GLOBALS['con'], $houseKeeping2);
    
$apiKey = RandomString();    

//make a new api entry node for this server
    $structure = './api/'.$serverKey;
if (!mkdir($structure, 0777, true)) {
    die('Failed to create folders...');
}
$newConf = "<?php 
\$secret = '".$apiKey."'; 
\$dbtable = '".$serverKey."';
?>";
$newPath = "./api/".$serverKey."/index.php";
$newConfDir = "./api/".$serverKey."/vars.php";
copy("./data.php",$newPath);

$myfile = fopen($newConfDir, "w") or die("Unable to open file!");

fwrite($myfile, $newConf);

fclose($myfile);
 

    
$message = "Setup Complete!";
}

if ($_GET['a']=="del"){
    $id = $_GET['id'];
    $sql = "DELETE FROM `$serverKey` WHERE id=$id";

    if (mysqli_query($GLOBALS['con'], $sql)) {
        $message = "Record deleted successfully";
    } else {
        $message = "Error deleting record: " . mysqli_error($conn);
    }
}
if ($_GET['a']=="clear"){
    //TRUNCATE TABLE table_name;
    $sql = "TRUNCATE TABLE `$serverKey`";

    if (mysqli_query($GLOBALS['con'], $sql)) {
        $message = "Database Cleared successfully";
    } else {
        $message = "Error deleting record: " . mysqli_error($conn);
    }    
    
    
}

?>
   
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>[QuestServer]</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <link rel="stylesheet" href="assets/css/Header-Dark.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Dark.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
    <div>
        <div class="header-dark">
            <nav class="navbar navbar-default navigation-clean-search">
                <div class="container">
                    <div class="navbar-header"><a class="navbar-brand navbar-link" href="#">[Quest Server]</a>
                        <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                    </div>
                    <div class="collapse navbar-collapse" id="navcol-1">
                        <ul class="nav navbar-nav">
                        </ul>
                        <form class="navbar-form navbar-left" target="_self">
                            <div class="form-group">
                                <label class="control-label" for="search-field"><i class="glyphicon glyphicon-search"></i></label>
                                <input class="form-control search-field" type="search" name="search" id="search-field">
                            </div>
                        </form>
                        <p class="navbar-text navbar-right"> <a class="btn btn-danger delete" role="button" <?php echo("href=\"?token=".$token."&a=clear".$row['id']."\""); ?>>Clear Database</a></p>
                    </div>
                </div>
            </nav>
            <div class="container hero">
                <div class="row">
                   <div class="col-md-12">
                       <p class="align-center"><?php echo ($message.", ".$slName);?></p>
                   </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="text-center">Database Entries</h1>
                        <div class="embed-responsive embed-responsive-16by9">
                            <div classs="well embed-responsive-item">
                                <table class="table">
                                  <thead>
                                    <tr>
<!--                                      <th scope="col">#</th>-->
                                      <th scope="col">Resident UUID</th>
                                      <th scope="col">Progress Bits</th>
                                      <th scope="col">Timestamp</th>
                                      <th scope="col">Actions</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <!-- Table Code Here -->
                                    <?php
                                      if ($message == "Setup Complete!"){
                                          
                                          echo ("<h3>Almost there...</h3>");
                                          echo ("<p>Please edit the config notecard in your server and paste the following at the top: -");
                                          echo ("<div class=\"well\">");
                                          echo ("<p>apiKey = ".$apiKey."</p>");
                                          echo ("<p>cipher = ".RandomString()."</p>");
                                          echo ("</div>");
                                          
                                      } else {
                                      
                                        $result= mysqli_query($GLOBALS['con'],"SELECT * FROM `$serverKey`");
                                        while($row = mysqli_fetch_array($result)){
                                            echo "<tr>";
                                            //echo "<th scope=\"row\">".$row['id']."</th>";
                                            echo "<td>".$row['uuid']."</td>";
                                            echo "<td>".$row['value']."</td>";
                                            echo "<td>".$row['timestamp']."</td>";
                                            echo "<td><a class=\"delete\" style=\"color: #e74c3c\" href=\"?token=".$token."&a=del&id=".$row['id']."\"><span class=\"glyphicon glyphicon-remove-sign\" aria-hidden=\"true\"></span>Remove</a>"."</td>";  
                                            echo "</tr>";
                                        }
                                      }
                                    ?>
                                    <!-- End Table code --> 
                                  </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/functions.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>
<?php 
mysqli_close($GLOBALS['con']);
?>
